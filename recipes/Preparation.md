# Preparation for Good-taste
 Some preparations are ready to good-taste.

 |Yaki-imo DX system overview|
 |---|
 |![IMG_1764](/uploads/6c209cf3d1208ebea964ff18fe352a76/IMG_1764.jpeg)|

## Customized toaster-oven  
 Customizing points are bypassing thermo-stat and timer-switch. AC-Power is controlled by Solid State Relay (SSR). SSR is controlled by GPIO of [Groovy-SPI]().  Of-course, you can use **not-customized** toaster-oven system with your manual operation such as timer setting (avoidance thermo-stat will be difficult...).

|Original circuit|Bypassing timer-switch and thermo-stat|
|---|---|
|![IMG_1692](/uploads/1b175ee739f560d0a63dc2e7faf2155e/IMG_1692.jpeg)|![IMG_1698](/uploads/209f787623c95cabaeb4ba79093e7710/IMG_1698.jpeg)|
|Simple switch|Can be returned to normal functionality by switch|

## Recipes
 Yaki-imo DX advocates "Recipe as code". You can freely delivery the **Recipes** under MIT license. So far, only sample recipes are provided for Yaki-imo DX system confirmation.

## Sweet-potato(s) self
 Your favorite sweet potato breeds should be prepaired. My fovarite is [紅はるか(Beni-Haruka)](https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%84%E3%83%9E%E3%82%A4%E3%83%A2)

1. Wash  
2. Cut both-edge
3. Foiled

## Options
 Options will help more sweety.  

### Twin sensors
 Usually, one sensor is mandatory for heat/temparature control. It is equipped on back panel of toaster-oven. Groovy-SPI can handle one more sensor for Object-core. More preciously measure heat/temparature control as well.
|Sensor location|Back-panel view|Using|
|---|---|---|
|![IMG_1880](/uploads/7f74c4ad5ae0aa7fe67ff02a61665c9f/IMG_1880.jpeg)|![IMG_1879](/uploads/e4f772b37478d39e55bfa46cdf03a403/IMG_1879.jpeg)|![IMG_1896](/uploads/9a4543772addaaa6cc824edfa6ad5221/IMG_1896.jpeg)|


### Steel-plate
 For avoiding direct radiation-heat from heater and making far-infra-red heating, iron plate should be put under foiled sweet-potato and its self.

|Steel-plate 1|Steel-plate 2|
|---|---|
|![IMG_1901](/uploads/ba0bea3fcfb3e1a6efbe6fe4fe5b4f94/IMG_1901.jpeg)|![IMG_1897](/uploads/84278ef03e608c5b8640fe4e9777e19e/IMG_1897.jpeg)|
|Originally from Tiny BBQ Plate |Covered|



