# オーブントースター・サーモスタット 温度特性試験報告
 本試験報告はオーブントースター・サーモスタット 温度特性に関するものである。

## 免責事項
　本試験に関しては各機器の製造元ならびに本試験報告者にはいかなる責任も無いものとする。

## はじめに
　"焼き芋DX"を実現するための設備として、外部制御可能なオーブントースタを構築する必要がある。初めて使う機材も多いので、試用を兼ねて各種の確認試験を行う。本試験もその一環である。

## 試験目的  
　オーブントースターのサーモスタットは加熱を防ぐ目的で設置されている。その起動温度を持って、トースターの耐熱温度とも捉えることが出来る。つまり、起動温度未満であれば、トースターを安全に運用することが出来るので、今後の温度制御においての上限温度の目安となる。その起動温度の確認とその上限温度の的確な制御を認識することが本試験の目的である。

|オーブントースター内のサーモスタット|
|---|
|![IMG_1693](/uploads/2bcfcb4b72c615b5bc912cfaa97ba97d/IMG_1693.jpeg)|
|タッピングビスで横壁面にネジ止めされている|  


## 使用機材／機器
　以下の機材／機器を利用する。一部、使用機材／機器の詳細仕様の記述の代わりに、各ベンダへのURLをもって代わりとする。

### [SSR]()
　SSR。放熱の最適化により40Aまで制御可能。

### USBホストデバイス
　PC, Mac, LinuxのどれかのOSが動作し、CDC-ACMをドライバとして認識する機能を有するデバイス。本試験ではアンドロイドスマートフォンを用いた。

### [Groovy-IoT]()
　大宮技研製の多目的インターフェイス。本試験ではUART機能を利用する。

### Groovy-SPI (本段階では試作機)
　GPIO(General Purpose IO)とSPIの制御に特化したインターフェイスボード。"焼き芋DX"では、温度取得を目的としたSPIプログラムが実装されている。  

### MAX6675熱電対
　市販されているMAX6675熱電対を用いて、温度計測を行う

### オーブントースター
 定格1000Wのオーブントースター、実際に使用するトースター。

## 実験方法
　オーブントースターを空焚きし、サーモスタットが働いた時の温度を実際にトースターに設置した熱電対で計測する。この時、ヒーターがサーモスタットにより切れる温度と温度低下により復帰した温度を記録する。

## 実験結果
　ヒータの電源断温度は170℃、復帰温度は150℃であった。

## 考察
　無改造オーブントースターの実力は170℃近傍であるので、これ以上の温度が必要な場合は安全を確保した上でサーモスタットを撤去し、所望の温度に達するように改造する必要がある。
